/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.deneme.datastore;

import com.mycompany.deneme.domain.User;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

/**
 *
 * @author molgun, mrcyborg
 */
@ApplicationScoped
@ManagedBean
public class UserDatastore implements Serializable {
    
    private int count;
    private List<User> users;

    public UserDatastore() {
    }

    @PostConstruct
    public void init() {
        System.out.println("Inite Girdi");
        users = new ArrayList<User>();
        count = 0;
    }

    public List<User> getUsers() {
        return users;
    }
    
    public void addUser(User user) {
        count = count + 1;
        user.setId(count);
        users.add(user);
    }

    public void remove(int id) {
        for (User user : users) {
            if (user.getId() == id) {
                users.remove(user);
            }
            break;
        }
    }

}
