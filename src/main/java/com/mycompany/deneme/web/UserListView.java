/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.deneme.web;

import com.mycompany.deneme.datastore.UserDatastore;
import com.mycompany.deneme.domain.User;
import java.io.Serializable;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author molgun,mrcyborg
 */
@ViewScoped
@ManagedBean
public class UserListView implements Serializable{

    @ManagedProperty("#{userDatastore}")
    private UserDatastore dataStore;
    
    public UserListView() {
    }

    public List<User> getUsers() {
        return dataStore.getUsers();
    }

    public void setDataStore(UserDatastore dataStore) {
        this.dataStore = dataStore;
    }
}
