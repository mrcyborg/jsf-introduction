/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.deneme.web;

import com.mycompany.deneme.datastore.UserDatastore;
import com.mycompany.deneme.domain.User;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author molgun,mrcyborg
 */
@ManagedBean
@ViewScoped
public class HelloView implements Serializable {

    @ManagedProperty("#{userDatastore}")
    private UserDatastore userDatastore;
    private User user;

    public HelloView() {
    }

    @PostConstruct
    public void init() {
        user = new User();
    }

    public String save() {
        userDatastore.addUser(user);
        return "index.xhtml?faces-redirect=true";
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setUserDatastore(UserDatastore userDatastore) {
        this.userDatastore = userDatastore;
    }
}
